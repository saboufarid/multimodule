package com.digihapi.multimodules.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digihapi.multimodules.model.Employee;
import com.digihapi.multimodules.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;
    
    @Override
	public Optional<Employee> getEmployee(final Long id) {
        return employeeRepository.findById(id);
    }

    @Override
	public Iterable<Employee> getEmployees() {
        return employeeRepository.findAll();
    }

    @Override
	public void deleteEmployee(final Long id) {
        employeeRepository.deleteById(id);
    }

    @Override
	public Employee saveEmployee(Employee employee) {
        Employee savedEmployee = employeeRepository.save(employee);
        return savedEmployee;
    }
    
}
