package com.digihapi.multimodules.service;

import java.util.Optional;

import com.digihapi.multimodules.model.Employee;

public interface EmployeeService {

	Optional<Employee> getEmployee(Long id);

	Iterable<Employee> getEmployees();

	void deleteEmployee(Long id);

	Employee saveEmployee(Employee employee);

}