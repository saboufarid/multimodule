package com.digihapi.multimodules.logs;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "com.digihapi.logs")
public class CustomProperties {

	private boolean requestLogged = false;
    private boolean requestBodyLogged = false;
    private boolean responseLogged = false;
    private boolean responseBodyLogged = false;
    
	public boolean isRequestLogged() {
		return requestLogged;
	}

	public void setRequestLogged(boolean requestLogged) {
		this.requestLogged = requestLogged;
	}
    
	public boolean isRequestBodyLogged() {
		return requestBodyLogged;
	}
	
	public void setRequestBodyLogged(boolean requestBodyLogged) {
		this.requestBodyLogged = requestBodyLogged;
	}

	public boolean isResponseLogged() {
		return responseLogged;
	}

	public void setResponseLogged(boolean responseLogged) {
		this.responseLogged = responseLogged;
	}

	public boolean isResponseBodyLogged() {
		return responseBodyLogged;
	}

	public void setResponseBodyLogged(boolean responseBodyLogged) {
		this.responseBodyLogged = responseBodyLogged;
	}
	
}