package com.digihapi.multimodules.logs.filter;

import java.io.IOException;
import java.util.Enumeration;
import java.util.UUID;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CustomURLFilter implements Filter {

	private static Logger log = LoggerFactory.getLogger(CustomURLFilter.class);
	private static final String REQUEST_ID = "request_id";
	
	private boolean requestLogged = true;
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {

		String requestId = UUID.randomUUID().toString();
		servletRequest.setAttribute(REQUEST_ID, requestId);
		
		if (isRequestLogged()) {
			logRequest((HttpServletRequest) servletRequest, requestId);
		}
		filterChain.doFilter(servletRequest, servletResponse);
	}

	@Override
	public void destroy() {

	}

	private void logRequest(HttpServletRequest request, String requestId) {
		if (request != null) {
			StringBuilder data = new StringBuilder();
			data.append("\nLOGGING REQUEST-----------------------------------\n").append("[REQUEST-ID]: ")
					.append(requestId).append("\n").append("[PATH]: ").append(request.getRequestURI()).append("\n")
					.append("[QUERIES]: ").append(request.getQueryString()).append("\n").append("[HEADERS]: ")
					.append("\n");

			Enumeration<String> headerNames = request.getHeaderNames();
			while (headerNames.hasMoreElements()) {
				String key = headerNames.nextElement();
				String value = request.getHeader(key);
				data.append("---").append(key).append(" : ").append(value).append("\n");
			}
			data.append("LOGGING REQUEST-----------------------------------\n");

			log.info(data.toString());
		}
	}

	public boolean isRequestLogged() {
		return requestLogged;
	}

	public void setRequestLogged(boolean requestLogged) {
		this.requestLogged = requestLogged;
	}
}
