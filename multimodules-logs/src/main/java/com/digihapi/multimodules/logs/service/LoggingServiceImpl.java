package com.digihapi.multimodules.logs.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digihapi.multimodules.logs.CustomProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class LoggingServiceImpl implements LoggingService {
	private static Logger log = LoggerFactory.getLogger(LoggingServiceImpl.class);
	private static final String REQUEST_ID = "request_id";

	@Autowired
	private CustomProperties customProperties;

	private String objectToString(Object object) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			return objectMapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			return "";
		}
	}

	@Override
	public void logRequest(HttpServletRequest httpServletRequest, Object body) {
		if (httpServletRequest.getRequestURI().contains("medias") || !customProperties.isRequestBodyLogged()) {
			return;
		}
		Object requestId = httpServletRequest.getAttribute(REQUEST_ID);
		StringBuilder data = new StringBuilder();
		data.append("\nLOGGING REQUEST BODY-----------------------------------\n").append("[REQUEST-ID]: ")
				.append(requestId).append("\n").append("[BODY REQUEST]: ").append("\n\n").append(objectToString(body))
				.append("\n\n").append("LOGGING REQUEST BODY-----------------------------------\n");

		log.info(data.toString());
	}

	@Override
	public void logResponse(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			Object body) {
		if (httpServletRequest.getRequestURI().contains("medias") || !customProperties.isResponseLogged()) {
			return;
		}
		Object requestId = httpServletRequest.getAttribute(REQUEST_ID);
		StringBuilder data = new StringBuilder();
		data.append("\nLOGGING RESPONSE-----------------------------------\n").append("[REQUEST-ID]: ")
				.append(requestId).append("\n");
		if (customProperties.isResponseBodyLogged()) {
			data.append("[BODY RESPONSE]: ").append(objectToString(body)).append("\n");
		}
		data.append("[STATUS]: ").append(httpServletResponse.getStatus()).append("\n")
				.append("LOGGING RESPONSE-----------------------------------\n");

		log.info(data.toString());
	}
}
