package com.digihapi.multimodules.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.digihapi.multimodules.model.Employee;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Long> {

}