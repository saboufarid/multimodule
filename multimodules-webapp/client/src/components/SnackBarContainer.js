import { Fragment, useEffect, useState } from "react";

// @material-ui/icons
import AddAlert from "@material-ui/icons/AddAlert";
import CheckCircle from "@material-ui/icons/CheckCircle";

import Snackbar from "./Snackbar/Snackbar";
import { useDispatch, useSelector } from "react-redux";
import { clear } from "../reducers/snackMsgs";

export default function SnackBarContainer({ place = "br", ...props }) {
  const { info, error } = useSelector(state => state.snackMsgs);
  const dispatch = useDispatch();

  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState(null);
  const [color, setColor] = useState("info");
  const [icon, setIcon] = useState(CheckCircle);

  useEffect(() => {
    if (info || error) {
      setOpen(true);
      setMessage(info ? info : error);
      setColor(info ? "info" : "danger");
      setIcon(info ? CheckCircle : AddAlert);
      dispatch(clear());
      if (info) {
        setTimeout(() => setOpen(false), 3000);
      }
    }
  }, [dispatch, info, error]);

  return (
    <Fragment>
      {props.children}
      <Snackbar
        place={place}
        color={color}
        icon={icon}
        message={message ? message : ""}
        open={open}
        closeNotification={() => setOpen(false)}
        close
      />
    </Fragment>
  );
}
