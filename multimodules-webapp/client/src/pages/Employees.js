import React, { useEffect } from "react";
import ReactTable from "react-table";
import "react-table/react-table.css";
import Delete from "@material-ui/icons/Delete";
import Edit from "@material-ui/icons/Edit";
import { Button } from "@material-ui/core";
import { useForm, Form } from "../components/useForm";
import Controls from "../components/controls/Controls";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchAddEmployee,
  fetchDeleteEmployee,
  fetchGetEmployees,
  fetchUpdateEmployee,
  initialValues,
  validate
} from "../reducers/employees";
import SnackBarContainer from "../components/SnackBarContainer";

const Employees = () => {
  const dispatch = useDispatch();
  const { employees, loading } = useSelector(state => state.employees);
  const { info } = useSelector(state => state.snackMsgs);

  useEffect(() => {
    dispatch(fetchGetEmployees());
    // eslint-disable-next-line
  }, []);

  const { values, setValues, errors, setErrors, handleInputChange, resetForm } =
    useForm(initialValues, false, validate);

  useEffect(() => {
    if (info) {
      resetForm();
    }
  }, [info, resetForm]);

  const handleSubmit = e => {
    e.preventDefault();
    const err = {
      ...errors,
      ...validate(values)
    };
    setErrors(err);
    if (Object.keys(err).length === 0) {
      dispatch(
        values.id ? fetchUpdateEmployee(values) : fetchAddEmployee(values)
      );
    }
  };

  const getEmployeesData = () => {
    const result = employees.map(
      ({ id, firstName, lastName, mail, password }) => {
        return {
          id,
          firstName,
          lastName,
          mail,
          password,
          actions: (
            <div className="actions-right">
              <Button
                onClick={() => {
                  resetForm();
                  setValues({
                    id,
                    firstName,
                    lastName,
                    mail,
                    password
                  });
                }}
              >
                <Edit color="primary" />
              </Button>
              <Button onClick={() => dispatch(fetchDeleteEmployee(id))}>
                <Delete color="error" />
              </Button>
            </div>
          )
        };
      }
    );

    return result;
  };

  return (
    <SnackBarContainer>
      <div className="App">
        {/* <header className="App-header"> */}
        <h1>Liste des employés</h1>
        <div className="Table">
          <ReactTable
            data={employees}
            resolveData={() => getEmployeesData()}
            // filterable
            columns={[
              {
                Header: "Id",
                accessor: "id",
                sortable: true,
                filterable: false
              },
              {
                Header: "Prénom",
                accessor: "firstName",
                sortable: true,
                filterable: false
              },
              {
                Header: "Nom",
                accessor: "lastName",
                sortable: true,
                filterable: false
              },
              {
                Header: "Email",
                accessor: "mail",
                sortable: false,
                filterable: false
              },
              {
                Header: "Mot de passe",
                accessor: "password",
                sortable: false,
                filterable: false
              },
              {
                Header: "Actions",
                accessor: "actions",
                sortable: false,
                filterable: false
              }
            ]}
            loading={loading}
            defaultPageSize={5}
            previousText="Précédent"
            nextText="Suivant"
            rowsText="Lignes"
            noDataText="Aucuns employés"
            ofText="sur"
            loadingText="Chargement..."
            className="-striped -highlight"
          />
        </div>

        <Form className="form" onSubmit={handleSubmit}>
          {values.id && (
            <Controls.Input
              className="input"
              name="id"
              label="id"
              value={values.id}
              disabled
            />
          )}
          <Controls.Input
            className="input"
            name="firstName"
            label="Prénom *"
            value={values.firstName}
            onChange={handleInputChange}
            error={errors.firstName}
          />
          <Controls.Input
            className="input"
            name="lastName"
            label="Nom *"
            value={values.lastName}
            onChange={handleInputChange}
            error={errors.lastName}
          />
          <Controls.Input
            className="input"
            name="mail"
            label="Email *"
            value={values.mail}
            onChange={handleInputChange}
            error={errors.mail}
          />
          <Controls.Input
            className="input"
            name="password"
            label="Mot de passe *"
            value={values.password}
            onChange={handleInputChange}
            error={errors.password}
          />
          <div>
            <Controls.Button
              className="cancel"
              color="secondary"
              onClick={resetForm}
              text="Annuler"
            />
            <Controls.Button
              type="submit"
              text={values.id ? "Modifier" : "Ajouter"}
            />
          </div>
        </Form>
      </div>
    </SnackBarContainer>
  );
};

export default Employees;
