import React from "react";
import "./App.css";
import { createTheme, ThemeProvider } from "@material-ui/core";

import Employees from "./pages/Employees";

const theme = createTheme({
  palette: {
    primary: {
      // main: "#333996",
      main: "#664433",
      light: "#3c44b126"
    },
    secondary: {
      main: "#ccc",
      light: "#f8324526"
    },
    background: {
      default: "#f8324526"
    }
  },
  overrides: {
    MuiAppBar: {
      root: {
        transform: "translateZ(0)"
      }
    }
  },
  props: {
    MuiIconButton: {
      disableRipple: true
    }
  }
});

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Employees />
    </ThemeProvider>
  );
}

export default App;
