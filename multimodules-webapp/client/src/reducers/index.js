import { combineReducers } from "redux";
import employees from "./employees";
import snackMsgs from "./snackMsgs";

export default function createReducers() {
  const appReducer = combineReducers({
    employees,
    snackMsgs
  });
  return appReducer;
}
