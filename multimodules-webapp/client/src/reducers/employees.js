import { createSlice } from "@reduxjs/toolkit";
import { api } from "../actions/api";
import { infoMsg } from "./snackMsgs";

export const initialValues = {
  id: "",
  firstName: "",
  lastName: "",
  mail: "",
  password: ""
};

export const validate = values => {
  const errors = {};
  for (const key of Object.keys(values)) {
    switch (key) {
      case "firstName":
        if (!values.firstName) errors.firstName = "Le prénom est obligatoire !";
        break;
      case "lastName":
        if (!values.lastName) errors.lastName = "Le nom est obligatoire !";
        break;
      case "mail":
        if (!values.mail) {
          errors.mail = "L'email est obligatoire !";
        } else if (!/$^|.+@.+..+/.test(values.mail)) {
          errors.mail = "L'email est invalide !";
        }
        break;
      case "password":
        if (!values.password) {
          errors.password = "Le mot de passe est obligatoire !";
        } else if (values.password.length < 8) {
          errors.password = "Le mot de passe nécessite 8 caractères minimum !";
        }
        break;
      default:
        // do nothing
        break;
    }
  }
  return errors;
};

const slice = createSlice({
  name: "employees",
  initialState: {
    employees: [],
    loading: false
  },
  reducers: {
    loading: (state, action) => {
      state.loading = action.payload;
    },
    loadSuccess: (state, action) => {
      state.employees = action.payload;
    }
  }
});

export default slice.reducer;

// ACTIONS
const { loading, loadSuccess } = slice.actions;

export const fetchGetEmployees = () =>
  api({
    onLoading: loading,
    url: "/employees",
    method: "GET",
    onSuccess: loadSuccess
  });

export const fetchAddEmployee = data =>
  api({
    onLoading: loading,
    url: "/employee",
    method: "POST",
    data,
    onSuccess: () => [
      infoMsg("L'employé a été ajouté avec succès"),
      fetchGetEmployees()
    ]
  });

export const fetchUpdateEmployee = data =>
  api({
    onLoading: loading,
    url: `/employee/${data.id}`,
    method: "PUT",
    data,
    onSuccess: () => [
      infoMsg("L'employé a été modifié avec succès"),
      fetchGetEmployees()
    ]
  });

export const fetchDeleteEmployee = id =>
  api({
    onLoading: loading,
    url: `/employee/${id}`,
    method: "DELETE",
    onSuccess: () => [
      infoMsg("L'employé a été supprimé avec succès"),
      fetchGetEmployees()
    ]
  });
