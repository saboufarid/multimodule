import { createSlice } from "@reduxjs/toolkit";

const slice = createSlice({
  name: "snackMsgs",
  initialState: {
    info: null,
    error: null,
    initialError: null
  },
  reducers: {
    clear: state => {
      state.info = null;
      state.error = null;
      state.initialError = null;
    },
    info: (state, action) => {
      state.info = action.payload;
    },
    error: {
      // prepare let us modify payload
      prepare: (error, initialError) => ({
        payload: { error, initialError }
      }),
      reducer: (draft, action) => {
        draft.error = action.payload.error;
        draft.initialError = action.payload.initialError;
      }
    }
  }
});

export default slice.reducer;

// ACTIONS
export const { clear, error: errorMsg, info: infoMsg } = slice.actions;
