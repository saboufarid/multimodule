import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import apiMiddleware from "../middlewares/api";
import createReducers from "../reducers";

export default function configureStore() {
  const appReducer = createReducers();

  const store = createStore(
    appReducer,
    composeWithDevTools(applyMiddleware(apiMiddleware))
  );

  return store;
}
