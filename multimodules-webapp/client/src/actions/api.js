import { createAction } from "@reduxjs/toolkit";
import { errorMsg } from "../reducers/snackMsgs";

const appBaseUrl = process.env.REACT_APP_BASE_URL || "http://localhost:9000";

const api = createAction(
  "api/call",
  ({
    customUrl = null,
    url = "",
    method = "GET",
    onLoading = null,
    data = null,
    onSuccess = () => {},
    onFailure = error => errorMsg("Une erreur inattendue est survenue", error),
    accessToken = null,
    label = "",
    headers = null,
    schema = null,
    withCredentials = false
  }) => ({
    payload: {
      url: customUrl ? customUrl : appBaseUrl + url,
      method,
      data,
      accessToken,
      onLoading,
      onSuccess,
      onFailure,
      label,
      headers,
      schema,
      withCredentials
    }
  })
);

const apiStart = createAction("api/start");
const apiEnd = createAction("api/end");
const apiError = createAction("api/error", (error, label) => ({
  error: error && error.message ? error.message : error,
  payload: label
}));
const apiAccessDenied = createAction("api/access_denied");

export { api, apiStart, apiEnd, apiError, apiAccessDenied };
