import axios from "axios";
import { api, apiError, apiStart, apiEnd } from "../actions/api";
import { errorMsg } from "../reducers/snackMsgs";

const apiMiddleware =
  ({ dispatch }) =>
  next =>
  action => {
    if (Array.isArray(action)) {
      action.forEach(obj => obj && dispatch(obj));
      return;
    }
    next(action);

    if (!api.match(action)) return;

    let {
      url,
      method,
      data,
      onLoading,
      onSuccess,
      onFailure,
      label,
      headers,
      withCredentials
    } = action.payload;
    const dataOrParams = ["GET", "DELETE"].includes(method) ? "params" : "data";

    // axios default configs
    axios.defaults.baseURL = process.env.REACT_APP_BASE_URL || "";
    axios.defaults.headers.common["Content-Type"] = "application/json";

    if (label) {
      dispatch(apiStart(label));
    }
    if (onLoading) {
      dispatch(onLoading(true));
    }

    axios
      .request({
        url,
        method,
        headers,
        [dataOrParams]: data,
        withCredentials
      })
      .then(({ data }) => {
        dispatch(onSuccess(data));
      })
      .catch(error => {
        if (label) {
          dispatch(apiError(error, label));
        }
        if (error.message === "Network Error") {
          dispatch(errorMsg("Impossible de joindre le serveur", error));
        } else {
          dispatch(onFailure(error));
        }
      })
      .then(() => {
        if (label) {
          dispatch(apiEnd(label));
        }
        if (onLoading) {
          dispatch(onLoading(false));
        }
      });
  };

export default apiMiddleware;
